#!/bin/sh

setup() {
  account_id=$1
  role_name=$2
  role_session_name=$3

  role_arn="arn:aws:iam::${account_id}:role/${role_name}"
}

assume_role() {
    credentials=($(aws sts assume-role \
          --role-arn "${role_arn}" \
          --role-session-name "${role_session_name}" \
          --query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
          --output text))

    access_key="${credentials[0]}"
    secret_key="${credentials[1]}"
    session_token="${credentials[2]}"
}

check() {
   if [ -z "$access_key" ] || [ -z "$secret_key" ] || [ -z "$session_token" ]; then
           echo "Failed to assume role. Please check your inputs and AWS configuration."
           return 1
       fi
}

export_credentials() {
     export AWS_ACCESS_KEY_ID=$access_key
     export AWS_SECRET_ACCESS_KEY=$secret_key
     export AWS_SESSION_TOKEN=$session_token
     echo "AWS credentials exported successfully."
}


main() {
  setup "$@"
  assume_role
  check
  export_credentials
}

main "$@"