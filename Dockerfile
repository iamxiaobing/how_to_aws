FROM openjdk:17-jdk-alpine as builder

ARG MAVEN_VERSION=3.5.3
WORKDIR /usr/src/app

RUN apk update && \
wget -O /tmp/apache-maven.tar.gz https://repo1.maven.org/maven2/org/apache/maven/apache-maven/$MAVEN_VERSION/apache-maven-$MAVEN_VERSION-bin.tar.gz && \
mkdir -p /usr/share/maven && \
tar -zxvf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 && \
rm -f /tmp/apache-maven.tar.gz && \
ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

COPY .. /usr/src/app

RUN mvn package

FROM openjdk:17

COPY --from=builder /usr/src/app/target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
